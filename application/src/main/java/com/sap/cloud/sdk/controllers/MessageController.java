package com.sap.cloud.sdk.controllers;

import org.slf4j.Logger;
import com.sap.cloud.sdk.*;
import com.sap.cloud.servicesdk.xbem.core.MessagingService;
import com.sap.cloud.servicesdk.xbem.core.MessagingServiceFactory;
import com.sap.cloud.servicesdk.xbem.core.exception.MessagingException;
import com.sap.cloud.servicesdk.xbem.core.impl.MessagingServiceFactoryCreator;
import com.sap.cloud.servicesdk.xbem.extension.sapcp.jms.MessagingServiceJmsConnectionFactory;
import com.sap.cloud.servicesdk.xbem.extension.sapcp.jms.MessagingServiceJmsSettings;
import com.sap.cloud.sdk.s4hana.datamodel.messaging.api.listener.*;
import com.sap.cloud.sdk.s4hana.datamodel.odata.services.*;
import com.sap.cloud.sdk.s4hana.datamodel.messaging.api.message.*;
import com.sap.cloud.sdk.s4hana.datamodel.odata.namespaces.businesspartner.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sap.cloud.sdk.cloudplatform.logging.CloudLoggerFactory;

import com.sap.cloud.sdk.models.HelloWorldResponse;

@RestController
@RequestMapping("/")
public class MessageController{
    private final SampleMessagingService messagingService;

    public MessageController(SampleMessagingService messagingService) {
        this.messagingService = messagingService;
    }

    @GetMapping("/listen")
    public void startReceiving() throws MessagingException {
        messagingService.startReceiving();
    }
}