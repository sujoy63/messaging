package com.sap.cloud.sdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.slf4j.Logger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.jms.*;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import com.sap.cloud.servicesdk.xbem.core.MessagingService;
import com.sap.cloud.servicesdk.xbem.core.MessagingServiceFactory;
import com.sap.cloud.servicesdk.xbem.core.exception.MessagingException;
import com.sap.cloud.servicesdk.xbem.core.impl.MessagingServiceFactoryCreator;
import com.sap.cloud.servicesdk.xbem.extension.sapcp.jms.MessagingServiceJmsConnectionFactory;
import com.sap.cloud.servicesdk.xbem.extension.sapcp.jms.MessagingServiceJmsSettings;
import com.sap.cloud.sdk.s4hana.datamodel.messaging.api.listener.*;
import com.sap.cloud.sdk.s4hana.datamodel.odata.services.*;
import com.sap.cloud.sdk.s4hana.datamodel.messaging.api.message.*;
import com.sap.cloud.sdk.s4hana.datamodel.odata.namespaces.businesspartner.*;

import org.slf4j.*;
import org.slf4j.LoggerFactory;



@Service
public final class SampleMessagingService {
	private static final Logger logger = CloudLoggerFactory.getLogger(SampleMessagingService.class);

	public static final int INITIAL_RECONNECT_DELAY = 3000;
	public static final int RECONNECT_DELAY = 3000;
	public static final int UNLIMITED_CONNECT_ATTEMPTS = -1;

	private static final String QUEUE_PREFIX = "queue:";
	private static final String QUEUE_NAME = "BUPA_CHANGED_QUEUE";
	private static final MessagingService messagingService = getMessagingService();

	private static MessagingService getMessagingService() {
	    final Cloud cloud = new CloudFactory().getCloud();
	    return cloud.getSingletonServiceConnector(MessagingService.class, null);
	}
	private Connection grantConnection() throws MessagingException, JMSException {
	    final MessagingServiceJmsSettings settings = new MessagingServiceJmsSettings();

	    settings.setMaxReconnectAttempts(UNLIMITED_CONNECT_ATTEMPTS);
	    settings.setInitialReconnectDelay(INITIAL_RECONNECT_DELAY);
	    settings.setReconnectDelay(RECONNECT_DELAY);

	    final MessagingServiceFactory messagingServiceFactory = MessagingServiceFactoryCreator.createFactory(messagingService);
	    final MessagingServiceJmsConnectionFactory connectionFactory = messagingServiceFactory.createConnectionFactory(MessagingServiceJmsConnectionFactory.class, settings);

	    return connectionFactory.createConnection();
	}

	private MessageConsumer getConsumer(final Session session) throws JMSException {
	    final Queue queue = session.createQueue(QUEUE_PREFIX + QUEUE_NAME);
	    return session.createConsumer(queue);
	}
	public void startReceiving() throws MessagingException {
	    try {
	        final Connection connection = grantConnection();

	        final Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

	        final MessageConsumer messageConsumer = getConsumer(session);

	        final JmsMessageDelegator delegator = new JmsMessageDelegator(new DefaultMessageDelegator());

	        delegator.addListener(new MyBusinessPartnerChangedLookupListener());

	        messageConsumer.setMessageListener(delegator);

	        connection.start();
	    } catch (final JMSException e) {
	        throw new MessagingException(e);
	    }
	}
	class MyBusinessPartnerChangedLookupListener implements BusinessPartnerChangedMessageListener {
	    @Override
	    private static final BusinessPartnerService businessPartnerService = new DefaultBusinessPartnerService();
	    
	    public void onMessage(@Nonnull final BusinessPartnerChangedMessage message) {
	        if (!message.getBusinessPartner().isPresent()) {
	            logger.error("Received Business Partner changed message w/o Business Partner key.");
	            return;
	        }
	  
	        logger.info("Received change of business partner " + message.getBusinessPartner().get());

	        lookupBusinessPartner(message.getBusinessPartner().get())
	                .onFailure(cause -> logger.error("Exception during Business Partner lookup.", cause))
	                .onSuccess(businessPartner -> logger.info("Fetched Business Partner " + businessPartner.getBusinessPartner() + " from S/4HANA"));
	    }
	    private Try<BusinessPartner> lookupBusinessPartner(final String businessPartnerKey) {
	        return Try.of(() -> new JwtBasedRequestContextExecutor()
	               .withXsuaaServiceJwt()
	               .execute(() -> {
	                   final BusinessPartner businessPartner =  businessPartnerService.getBusinessPartnerByKey(businessPartnerKey).execute();

	                   logger.info("Business partner: " + businessPartner);

	                   return businessPartner;
	               }));
	    }
	}
}